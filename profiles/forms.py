from django.forms import ModelForm
from .models import UserProfile, User


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        fields = ('avatar_url', 'phone_number', 'bio', 'location', 'birth_date')

