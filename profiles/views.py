from django.shortcuts import render
from django.views.generic import DetailView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from profiles.models import UserProfile, User
# Create your views here.


class ProfileView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = UserProfile
    template_name = 'profiles/profile_detail.html'
    raise_exception = True

    def test_func(self):
        return self.request.user.id == int(self.kwargs['pk'])


class ProfileUpdate(LoginRequiredMixin, UpdateView):
    model = UserProfile
    fields = ['avatar_url', 'phone_number', 'bio', 'location', 'birth_date']
    template_name = 'profiles/profile_update.html'

    def get_object(self, queryset=None):
        return UserProfile.objects.get(user=self.request.user)
