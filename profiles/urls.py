from django.conf.urls import url

from . import views

app_name = 'profiles'

urlpatterns = [
    url(r'^(?P<pk>\d+)/$', views.ProfileView.as_view(), name='profile'),
    url(r'^update/$', views.ProfileUpdate.as_view(), name='profile_update'),
]