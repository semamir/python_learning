from allauth.account.signals import user_signed_up
from django.dispatch import receiver
from .models import UserProfile
import hashlib


@receiver(user_signed_up)
def social_login_profilepic(sender, user, **kwargs):
    preferred_avatar_size_pixels = 256
    print(user)

    picture_url = "http://www.gravatar.com/avatar/{0}?s={1}".format(
        hashlib.md5(user.email.encode('UTF-8')).hexdigest(),
        preferred_avatar_size_pixels
    )

    if sender:
        sociallogin = sender

        try:
            if sociallogin.account.provider == 'facebook':
                pass
        except AttributeError:
            pass

    profile = UserProfile(user=user, avatar_url=picture_url)
    profile.save()




