from django.db import models
from django.contrib.auth.models import User
from allauth.account.models import EmailAddress
from allauth.socialaccount.models import SocialAccount
from django.urls import reverse


class UserProfile (models.Model):
    user = models.OneToOneField(User, primary_key=True, verbose_name='user', related_name='profile',
                                on_delete=models.CASCADE)
    avatar_url = models.CharField(max_length=256, blank=True, null=True)
    phone_number = models.CharField(max_length=15, blank=True)
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return "{}".format(self.user.email)

    class Meta:
        db_table = 'user_profile'

    def get_absolute_url(self):
        return reverse('profiles:profile', kwargs={'pk': self.user.id})